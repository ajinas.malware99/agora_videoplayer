import 'package:flutter/material.dart';
import 'package:agora_uikit/agora_uikit.dart';

import 'package:video_call/start_screen.dart';

class HomePage extends StatefulWidget {
  final AgoraClient client;

  const HomePage({Key? key, required this.client}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isCalling = false;

  @override
  void initState() {
    // _saveOptions();
    super.initState();

    initAgora();
  }

  void initAgora() async {
    await widget.client.initialize();

    setState(() {
      isCalling = true;
    });
  }

  Future<bool> customFeature() async {
    Navigator.pop(context);
    return false;
  }

  Future<bool> nextPage() async {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => StartScreen(isCalling)));
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: nextPage,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          title: const Text("---^66^^---m"),
        ),
        body: Stack(
          children: [
            AgoraVideoViewer(
              client: widget.client,
            ),
            AgoraVideoButtons(
              client: widget.client,
            )
          ],
        ),
      ),
    );
  }
}
