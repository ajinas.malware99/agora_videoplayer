import 'package:agora_uikit/agora_uikit.dart';
import 'package:flutter/material.dart';
import 'package:video_call/home_page.dart';
import 'package:draggable_widget/draggable_widget.dart';

class StartScreen extends StatefulWidget {
  late bool connection;

  StartScreen(this.connection, {Key? key})
      : super(
          key: key,
        );

  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  bool iscallConnected = false;
  AgoraClient client = AgoraClient(
    agoraConnectionData: AgoraConnectionData(
        appId: "b6bfc5e31e364f0985da733efb15ce32",
        channelName: "test",
        tempToken:
            "006b6bfc5e31e364f0985da733efb15ce32IABCPErEip+WX9I/9x+OiHysfurryHeik7IwCGfG59uEBQx+f9gAAAAAEADPE6q5ajCpYQEAAQBnMKlh"),
    enabledPermission: [Permission.camera, Permission.microphone],
  );
  final dragController = DragController();
  @override
  void initState() {
    setState(() {
      iscallConnected = widget.connection;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              FloatingActionButton.extended(
                onPressed: () {
                  iscallConnected == false
                      ? Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HomePage(
                              client: client,
                            ),
                          ))
                      : Navigator.pop(context);
                },
                label: iscallConnected == false
                    ? const Text("connect remote user")
                    : const Text("goto existing call"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
